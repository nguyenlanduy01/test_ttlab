const m = 13,
  d = 5,
  k = 1,
  c = 2;

function totalGold(m, d, k, c) {
  if (m <= d) {
    return 0;
  }
  if (k >= d) {
    return -1;
  }

  let numbersOfRepairs = Math.floor(m / Math.floor((d - 1) / k));
  if (
    m - numbersOfRepairs * Math.floor((d - 1) / k) != 0 &&
    m - numbersOfRepairs * Math.floor((d - 1) / k) > k
  ) {
    numbersOfRepairs += 1;
  }
  return (numbersOfRepairs - 1) * c;
}

console.log(totalGold(m, d, k, c));
