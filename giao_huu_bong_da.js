const teamA = 2,
  teamB = 1;

function factorial(n) {
  if (n === 0 || n === 1) {
    return 1;
  }
  return n * factorial(n - 1);
}

function numberOfWays(n, k) {
  return factorial(n) / (factorial(k) * factorial(n - k));
}

console.log(numberOfWays(teamA + teamB, teamA));
